# Generated from pwrmeter.org
#

from typing import (
    Optional, List, Dict, Tuple
)

from functools import (
    partial
)

import sys
import traceback
from time import sleep
import asyncio
import numpy as np
from argparse import ArgumentParser
import serial
from serial.tools.list_ports import comports

from rfblocks import (
    LogDetector, PwrDetectorController, pe43711, PE43711Controller,
    DetectorReadError, create_serial, write_cmd, query_cmd,
    DEFAULT_BAUDRATE, DEFAULT_SOCKET_URL
)

from qtrfblocks import (
    PowerDetector, StepAttenuator
)

from qasync import (
    QEventLoop, QThreadExecutor, asyncSlot, asyncClose
)

import pyvisa

from tam import (
    InstrumentMgr
)

from PyQt5.QtWidgets import (
    QApplication, QWidget, QMainWindow, QLabel, QPushButton,
    QVBoxLayout, QHBoxLayout, QFormLayout, QComboBox, QGroupBox,
    QErrorMessage, QDoubleSpinBox, QLineEdit, QButtonGroup, QRadioButton,
    QCheckBox
)

from PyQt5.QtCore import (
    Qt, QObject, QThread, pyqtSignal, pyqtSlot
)

import rpyc
from rpyc.utils.server import ThreadedServer

class PwrMeterApp(QMainWindow):

    CALSTATUS_INCAL = 0
    CALSTATUS_ESTIMATED = 1
    CALSTATUS_UNCAL = 2

    CALSTATUS_STR = {
        CALSTATUS_INCAL: 'INCAL',
        CALSTATUS_ESTIMATED: 'ESTIMATED',
        CALSTATUS_UNCAL: 'UNCAL'}
    CALSTATUS_COLOURS = {
        CALSTATUS_INCAL: 'black',
        CALSTATUS_ESTIMATED: 'orange',
        CALSTATUS_UNCAL: 'red'
    }

    calstatus_changed = pyqtSignal(int, int)

    def __init__(self,
                 chan_cspins: List = None,
                 atten_lepins: List = None,
                 serial_device: Optional[str] = None,
                 baudrate: int = 0,
                 headless: bool = False) -> None:
        """
        """
        super().__init__()
        self._nogui: bool = headless
        self._detectors: List[PwrDetectorController] = [
            PwrDetectorController('0', LogDetector(chan_cspins[0])),
            PwrDetectorController('1', LogDetector(chan_cspins[1]))]
        self._attenuators: List[Optional[PE43711Controller]] = [None, None]
        if atten_lepins:
            for att_id in[0, 1]:
                if atten_lepins[att_id]:
                    att_ctl = PE43711Controller(
                        str(att_id), pe43711(atten_lepins[att_id]))
                    att_ctl.attenuation_changed.connect(
                        self.attenuation_changed)
                    self._attenuators[att_id] = att_ctl
        self._continuous: bool = False
        self._acquiring: bool = False
        self._shuttingdown: bool = False
        self._acquire_group_box: Optional[QGroupBox] = None
        self._ctl_device: str = serial_device
        self._baudrate: int = baudrate
        if baudrate == 0:
            self._baudrate = DEFAULT_BAUDRATE
        self._calstatus: List[int] = [
            PwrMeterApp.CALSTATUS_UNCAL, PwrMeterApp.CALSTATUS_UNCAL]
        for det_id, ctl in enumerate(self._detectors):
            ctl.caldata_changed.connect(partial(self.caldata_updated, det_id))
        self.calstatus_changed.connect(self.update_calstatus)

        if not headless:
            self._detector_displays: List[Optional[PowerDetector]] = [
                PowerDetector(ctl) for ctl in self.detectors]
            self._attenuator_displays: List[Optional[StepAttenuator]] = [None, None]
            for att_id in [0, 1]:
                att = self.attenuators[att_id]
                if att:
                    self._attenuator_displays[att_id] = StepAttenuator(
                        self, att)
            self.init_ui()

    def chan0_power_offset_fn(self, freq):
        """
        """
        return self._coupling + self._attn_insertion_loss

    def chan1_power_offset_fn(self, freq):
        """
        """
        return self._amplifier_gain + self._coupling

    @property
    def ctl_device(self) -> str:
        return self._ctl_device

    @property
    def serial_ports(self) -> List:
        ports = [DEFAULT_SOCKET_URL]
        ports.extend([port.device for port in comports() if port.serial_number])
        if self.ctl_device:
            if self.ctl_device in ports:
                ports.pop(ports.index(self.ctl_device))
            ports.insert(0, self.ctl_device)
        return ports

    @property
    def baudrate(self) -> int:
        return self._baudrate

    @property
    def detectors(self) -> List[PwrDetectorController]:
        """Returns a list containing the power detector controllers.
        """
        return self._detectors

    @property
    def attenuators(self) -> List[Optional[PE43711Controller]]:
        """Returns a list containing the step attenuator controllers.
        """
        return self._attenuators


    @property
    def calstatus(self) -> List[int]:
        """Returns a list containing the status of the detectors.
        """
        return self._calstatus

    @pyqtSlot(object)
    def attenuation_changed(self, att_ctl: PE43711Controller) -> None:
        """Device attenuation setting has changed.
        """
        det_id = int(att_ctl._controller_id)
        ctl: PwrDetectorController = self.detectors[det_id]

        # Check the calibration attenuation setting.
        # If there is no calibration attenuation setting
        # then just return without doing anything
        try:
            cal_atten = ctl.cal_data_conditions[
                PwrDetectorController.ATTEN_CAL_CONDITION]
        except KeyError:
            # There's no calibration attenuation value so
            # just set the insertion loss offset
            ctl.insertion_loss_offset = att_ctl.attenuation
            return

        # If the current attenuation setting for att_ctl
        # is different from the calibration attenuation
        # then calculate the insertion loss offset.
        loss_offset: float = att_ctl.attenuation - cal_atten
        if ctl.insertion_loss_offset == 0.0 and loss_offset != 0.0:
            # If the current value of the detector
            # controller insertion_loss_offset is zero and
            # the calculated loss offset is non-zero
            # emit a calstatus_changed signal.
            self.calstatus_changed.emit(det_id, PwrMeterApp.CALSTATUS_ESTIMATED)
        elif ctl.insertion_loss_offset != 0.0 and loss_offset == 0.0:
            # Likewise, if the detector controller
            # insertion_loss_offset is non-zero and the
            # calculated loss offset is zero
            # emit a calstatus_changed signal.
            self.calstatus_changed.emit(det_id, PwrMeterApp.CALSTATUS_INCAL)
        # Set the detector controller insertion loss offset
        ctl.insertion_loss_offset = loss_offset

    def disable_chan_controls(self, chan_id: int, f: bool) -> None:
        if chan_id in [0, 1]:
            self._detector_displays[chan_id].disable_controls(f)
            att_display = self._attenuator_displays[chan_id]
            if att_display:
                att_display.disable_controls(f)

    def disable_controls(self, f: bool) -> None:
        for det_id in [0, 1]:
            self.disable_chan_controls(det_id, f)
        if self._acquire_group_box:
            self._acquire_group_box.setDisabled(f)

    def initialize_detectors(self) -> None:
        with create_serial(self.ctl_device,
                           self.baudrate) as ser:
            for ctl in self.detectors:
                ctl.initialize(ser)

    @pyqtSlot()
    def caldata_updated(self, det_id):
        self.update_calstatus(det_id, PwrMeterApp.CALSTATUS_INCAL)

    @pyqtSlot(int, int)
    def update_calstatus(self, det_id: int, status: int):
        """
        """
        print(f'update_calstatus: {det_id=}, {status=}')
        self.calstatus[det_id] = status
        if not self._nogui:
            grp: PowerDetector = self._detector_displays[det_id]
            grp._cal_status_lbl.setText(PwrMeterApp.CALSTATUS_STR[status])
            print(f'  {PwrMeterApp.CALSTATUS_COLOURS[status]=}')
            grp._cal_status_lbl.setStyleSheet(
                'QLabel { color: %s }' % PwrMeterApp.CALSTATUS_COLOURS[status])

    @property
    def continuous(self) -> bool:
        return self._continuous

    @property
    def acquiring(self) -> bool:
        return self._acquiring

    def set_continuous(self, c: bool) -> None:
        self._continuous = c

    def set_acquiring(self, a: bool) -> None:
        self._acquiring = a

    @property
    def shuttingdown(self) -> bool:
        return self._shuttingdown

    def shutdown(self, b: bool) -> None:
        self._shuttingdown = b

    def create_hwconfig_group(self):
        group_box = QGroupBox("Hardware Config:")
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        tty_combo = QComboBox()
        tty_combo.currentIndexChanged.connect(
            lambda idx, w=tty_combo: self.tty_changed(w, idx))
        tty_combo.addItems(self.serial_ports)
        line_edit = QLineEdit()
        line_edit.editingFinished.connect(
            lambda w=line_edit: self.tty_edit_finished(w))
        tty_combo.setLineEdit(line_edit)
        hbox.addWidget(QLabel("Control Port:"))
        hbox.addWidget(tty_combo)
        self.hw_config_btn = QPushButton("Initialize")
        self.hw_config_btn.clicked.connect(self.hw_config)
        hbox.addWidget(self.hw_config_btn)
        hbox.addStretch(1)
        vbox.addLayout(hbox)
        group_box.setLayout(vbox)
        return group_box
    
    def tty_changed(self, combo, idx):
        self._ctl_device = combo.itemText(idx)
    
    def tty_edit_finished(self, line_edit):
        self._ctl_device = line_edit.text()
    
    def showErrorMessage(self, msg):
        error_dialog = QErrorMessage(self)
        error_dialog.setWindowModality(Qt.WindowModal)
        error_dialog.setParent(self, Qt.Sheet)
        error_dialog.setResult(0)
        error_dialog.showMessage(msg)
    
    @asyncSlot()
    async def hw_config(self):
        self.hw_config_btn.setEnabled(False)
        control_disable = False
        try:
            loop = asyncio.get_event_loop()
            await loop.run_in_executor(None, self.initialize_detectors)
        except serial.serialutil.SerialException:
            self.showErrorMessage("Bad serial device.")
            control_disable = True
        except DetectorReadError:
            self.showErrorMessage("Error reading detector data.")
            control_disable = True
        except Exception as e:
            print(e)
            control_disable = True
        finally:
            self.hw_config_btn.setEnabled(True)
            self.disable_controls(control_disable)
    
    @asyncClose
    async def closeEvent(self, event):
        if self.continuous:
            self.set_acquiring(False)
            self.shutdown(True)
            event.ignore()
        else:
            event.accept()

    def create_acquisition_control_group(self):
        self._acquire_group_box = QGroupBox("Acquisition Control:")
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        acqGroup = QButtonGroup(hbox)
        rb = QRadioButton("Single")
        acqGroup.addButton(rb)
        rb.setChecked(True)
        hbox.addWidget(rb)
        rb = QRadioButton("Continuous")
        acqGroup.addButton(rb)
        hbox.addWidget(rb)
        rb.toggled.connect(lambda state: self.set_continuous(state))
        hbox.addStretch(1)
        self.acquire_btn = QPushButton("Acquire")
        self.acquire_btn.clicked.connect(self.acquire_pwr)
        hbox.addWidget(self.acquire_btn)
        vbox.addLayout(hbox)
        self._acquire_group_box.setLayout(vbox)
        return self._acquire_group_box
    
    def acquire_pwr_levels(self):
        with create_serial(self.ctl_device,
                           self.baudrate) as ser:
            if self.continuous:
                while self.acquiring:
                    for ctl in self.detectors:
                        ctl.measure(ser)
            else:
                for ctl in self.detectors:
                    ctl.measure(ser)
    
    def enable_stop_acquisition(self):
        self.disable_freq_controls()
        self.hw_config_btn.setEnabled(False)
        self.acquire_btn.setText("Stop")
        self.set_acquiring(True)
        try:
            self.acquire_btn.clicked.disconnect()
        except Exception:
            pass
        self.acquire_btn.clicked.connect(self.acquire_stop)
    
    def enable_start_acquisition(self):
        self.acquire_btn.setText("Acquire")
        try:
            self.acquire_btn.clicked.disconnect()
        except Exception:
            pass
        self.acquire_btn.clicked.connect(self.acquire_pwr)
        self.hw_config_btn.setEnabled(True)
        self.enable_freq_controls()
    
    def disable_freq_controls(self):
        for det in self._detector_displays:
            det._freq_box.setEnabled(False)
    
    def enable_freq_controls(self):
        for det in self._detector_displays:
            det._freq_box.setEnabled(True)
    
    @asyncSlot()
    async def acquire_pwr(self):
        loop = asyncio.get_event_loop()
        if self.continuous:
            self.enable_stop_acquisition()
            try:
                await loop.run_in_executor(None, self.acquire_pwr_levels)
                if self.shuttingdown:
                    QApplication.quit()
            except serial.serialutil.SerialException as se:
                print(se)
                self.showErrorMessage("Bad serial device.")
            except Exception as e:
                print(e)
            finally:
                self.enable_start_acquisition()
        else:
            self.acquire_btn.setEnabled(False)
            try:
                await loop.run_in_executor(None, self.acquire_pwr_levels)
            except serial.serialutil.SerialException:
                self.showErrorMessage("Bad serial device.")
            except Exception as e:
                print(e)
            finally:
                self.acquire_btn.setEnabled(True)
    
    @asyncSlot()
    async def acquire_stop(self):
        if self.continuous:
            self.set_acquiring(False)

    def calibrate(self, det_id, src_device, device_id, src_chan,
                  srcpwr, start, stop, step):
    
        print("calibrate: {}, {}, {}".format(src_device, device_id, src_chan))
        mgr = InstrumentMgr()
        sigsrc = mgr.open_instrument(src_device, device_id)
        try:
            sigsrc.chan = src_chan
        except AttributeError:
            pass
    
        loss = {}
        ctl = self.detectors[det_id]
        old_correct_setting = ctl.apply_correction
        ctl.apply_correction = False
        ctl.calibrating = True
        sigsrc.output = True
        with create_serial(self.ctl_device,
                           self.baudrate) as ser:
            for f in np.arange(start, stop, step):
                # Note that the property assignment here may actually
                # be a rpyc remote operation depending on the selected
                # calibration device.  Since ~f~ (and possibly ~srcpwr~)
                # may be numpy array scalars it is necessary to explicitly
                # cast these to standard Python float values.  If this is
                # not done the remote rpyc process will almost certainly
                # generate strange numpy related exceptions.
                sigsrc.freq = float(f)
                sigsrc.level = float(srcpwr)
                sleep(1.0)
                ctl.freq = f
                ctl.measure(ser)
                loss['{}'.format(f)] = srcpwr - ctl.pwr
    
        sigsrc.output = False
        ctl.calibrating = False
        ctl.apply_correction = old_correct_setting
        mgr.close_instrument(sigsrc)
        mgr.close()
    
        # If this detector has a step attenuator associated with it
        # then we must take note of the attenuation setting for which
        # the calibration was carried out. This is done by
        # saving the attenuation setting as a value in the
        # detector controller cal_data_conditions dictionary.
        att = self.attenuators[det_id]
        if att:
            ctl.cal_data_conditions[
                PwrDetectorController.ATTEN_CAL_CONDITION] = att.attenuation
            # Any current loss offset should now be set to 0 since it
            # is now corrected by the calibration
            ctl.insertion_loss_offset = 0.0
    
        ctl.cal_data = loss
        self.calstatus_changed.emit(det_id, PwrMeterApp.CALSTATUS_INCAL)
        return ''

    def init_ui(self) -> None:
        vbox = QVBoxLayout()
        vbox.addWidget(self.create_hwconfig_group())

        hbox = QHBoxLayout()
        for det_id in [0, 1]:
            inner_vbox = QVBoxLayout()
            grp = self._detector_displays[det_id].create_detector_group(
                partial(self.calibrate, det_id))
            inner_vbox.addWidget(grp)
            att_display = self._attenuator_displays[det_id]
            if att_display:
                grp = att_display.create_attenuator_group()
                inner_vbox.addWidget(grp)
            else:
                inner_vbox.addStretch()
            hbox.addLayout(inner_vbox)
        self.disable_controls(True)
        vbox.addLayout(hbox)

        hbox2 = QHBoxLayout()
        grp = self.create_acquisition_control_group()
        hbox2.addWidget(grp)
        grp.setDisabled(True)
        vbox.addLayout(hbox2)

        self.container = QWidget()
        self.container.setLayout(vbox)

        self.setCentralWidget(self.container)

        self.setWindowTitle('Dual Detector Power Meter')

class PwrMeterService(rpyc.Service):

    def __init__(self, app):
        super().__init__()
        self._app = app

    def initialize(self) -> None:
        with create_serial(self._app.ctl_device,
                           self._app.baudrate) as ser:
            for ctl in self.detectors:
                ctl.initialize(ser)
        if not self._app._nogui:
            self._app.hw_config_btn.setEnabled(True)
            self._app.disable_controls(False)


    @property
    def detectors(self):
        return self._app.detectors

    @property
    def attenuators(self):
        return self._app.attenuators

    def set_attenuation(self, ctl_id, atten):
        att = self.attenuators[ctl_id]
        if att:
            if not self._app._nogui:
                grp = self._app._attenuator_displays[ctl_id]
                if grp:
                    grp._atten_box.setValue(atten)
            else:
                att.attenuation = atten
            with create_serial(self._app.ctl_device,
                               self._app.baudrate) as ser:
                att.configure(ser)

    def detector_enable(self, ctl_id, en):
        if not self._app._nogui:
            state = Qt.Checked if en else Qt.Unchecked
            self._app._detector_displays[ctl_id].set_enabled(state)
            att_display = self._app._attenuator_displays[ctl_id]
            if att_display is not None:
                att_display.disable_controls(en)
        else:
            ctl = self.detectors[ctl_id].enabled = en

    def calibrate(self, ctl_id, src_dev, device_id, src_chan,
                  srcpwr, startf, stopf, stepf):
        return self._app.calibrate(
            ctl_id, src_dev, device_id, src_chan,
            srcpwr, startf, stopf, stepf)

    def save_caldata(self, ctl_id, cal_data_file):
        ctl = self.detectors[ctl_id]
        ctl.save_caldata(cal_data_file)

    def load_caldata(self, ctl_id, cal_data_file):
        ctl = self.detectors[ctl_id]
        ctl.load_caldata(cal_data_file)

    @property
    def calstatus(self):
        return self._app.calstatus

    def measure(self, ctl_id: int) -> None:
        with create_serial(self._app.ctl_device,
                           self._app.baudrate) as ser:
            self.detectors[ctl_id].measure(ser)


class RPyCServer(QObject):

    finished = pyqtSignal()

    def __init__(self, serviceInst, host, port):
        super().__init__()
        self._serviceInst = serviceInst
        self._host = host
        self._port = port

    def run(self):
        print("PwrMeter rpyc service on {}:{}".format(self._host, self._port))
        self._server = ThreadedServer(
            self._serviceInst,
            hostname = self._host,
            port = self._port,
            protocol_config = {
                'allow_all_attrs': True,
                'allow_setattr': True,
                'allow_pickle': True})
        self._server.start()
        self.finished.emit()


def main():
    global server_thread

    defaultBaud = 0
    
    parser = ArgumentParser(description=
                            '''An RF power meter.''')
    
    parser.add_argument(
        "--nogui", action='store_true',
        help="Disable GUI and run 'headless'")
    parser.add_argument(
        "-d", "--device", default=None,
        help="The hardware serial device")
    parser.add_argument(
        "-b", "--baudrate", default=defaultBaud, type=int,
        help="Baud rate (default: {})".format(defaultBaud))
    parser.add_argument(
        "-A", "--ipaddr", default="127.0.0.1",
        help="IP address for to bind the RPyC server instance")
    parser.add_argument(
        "-P", "--port", default=18863, type=int,
        help="TCP port for the RPyC server instance")
    parser.add_argument(
        "--chan0_cspin", default='D0',
        help="CS pin for Chan0 power detector. Default: D0")
    parser.add_argument(
        "--chan1_cspin", default='D1',
        help="CS pin for Chan1 power detector. Default: D1")
    parser.add_argument(
        "--atten0_lepin", default=None,
        help="LE pin for Chan0 step attenuator. Default: no attenuator")
    parser.add_argument(
        "--atten1_lepin", default=None,
        help="LE pin for Chan1 step attenuator. Default: no attenuator")
    args = parser.parse_args()
    

    app = QApplication(sys.argv)
    loop = QEventLoop(app)
    loop.set_default_executor(QThreadExecutor(1))
    asyncio.set_event_loop(loop)

    pwrmeter_app = PwrMeterApp(
        chan_cspins = [args.chan0_cspin, args.chan1_cspin],
        atten_lepins = [args.atten0_lepin, args.atten1_lepin],
        serial_device = args.device,
        baudrate = args.baudrate,
        headless = args.nogui)

    if not args.nogui:
        pwrmeter_app.show()

    server_thread = QThread()
    server = RPyCServer(PwrMeterService(pwrmeter_app),
                        args.ipaddr,
                        args.port)
    server.moveToThread(server_thread)
    server_thread.started.connect(server.run)
    server.finished.connect(server_thread.quit)
    server.finished.connect(server.deleteLater)
    server_thread.finished.connect(server_thread.deleteLater)
    server_thread.start()

    with loop:
        loop.set_debug(True)
        sys.exit(loop.run_forever())

if __name__ == '__main__':
    main()
